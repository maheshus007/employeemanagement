import React, { Component } from 'react'
import EmployeeService from '../services/EmployeeService'
import Pagination from "react-js-pagination";

class ListEmployeeComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activePage: 5
        };

        this.state = {
            employees: []
        }
        this.addEmployee = this.addEmployee.bind(this);
    }

    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        this.setState({ activePage: pageNumber });
        EmployeeService.getEmployees(pageNumber-1).then((res) => {
            this.setState({ employees: res.data });
        });
    }

    componentDidMount() {
        EmployeeService.getEmployees().then((res) => {
            this.setState({ employees: res.data });
        });
    }

    addEmployee() {
        this.props.history.push('/add-employee/_add');
    }

    render() {
        const DATE_OPTIONS = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
        return (
            <div>
                <h2 className="text-center">Employees List</h2>
                <div className="row">
                    <button className="btn btn-primary" onClick={this.addEmployee}> Add Employee</button>
                </div>
                <br></br>
                <div className="row">
                    <table className="table table-striped table-bordered">

                        <thead>
                            <tr>
                                <th> Employee First Name</th>
                                <th> Employee Last Name</th>
                                <th> Email</th>
                                <th> Phone</th>
                                <th> Salary</th>
                                <th> Manager Id</th>
                                <th> Hire date</th>
                                <th> Department Id</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.employees.map(
                                    employee =>
                                        <tr key={employee.id}>
                                            <td> {employee.firstName} </td>
                                            <td> {employee.lastName}</td>
                                            <td> {employee.email}</td>
                                            <td> {employee.phoneNumber}</td>
                                            <td> {employee.salary}</td>
                                            <td> {employee.managerId}</td>
                                            <td> {(new Date(employee.hireDate)).toLocaleDateString('en-US', DATE_OPTIONS)}</td>
                                            <td> {employee.departmentId}</td>
                                        </tr>
                                )
                            }
                        </tbody>
                    </table>

                </div>
                <Pagination
                    activePage={this.state.activePage}
                    itemsCountPerPage={10}
                    totalItemsCount={450}
                    pageRangeDisplayed={5}
                    onChange={this.handlePageChange.bind(this)}
                    itemClass='page-item'
                    linkClass='page-link'
                />
            </div>
        )
    }
}

export default ListEmployeeComponent
