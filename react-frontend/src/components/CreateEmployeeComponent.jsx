import React, { Component } from 'react'
import EmployeeService from '../services/EmployeeService';

class CreateEmployeeComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            fname: '',
            lname: '',
            email: '',
            phoneNumber: '',
            departmentId: '',
            hireDate: '',
            salary: ''
        }
        this.changeFirstNameHandler = this.changeFirstNameHandler.bind(this);
        this.changeLastNameHandler = this.changeLastNameHandler.bind(this);
        this.changeDeptIdHandler = this.changeDeptIdHandler.bind(this);
        this.changeEmailHandler = this.changeEmailHandler.bind(this);
        this.changeHireDateHandler = this.changeHireDateHandler.bind(this);
        this.changeManagerIdHandler = this.changeManagerIdHandler.bind(this);
        this.changePhoneNumberHandler = this.changePhoneNumberHandler.bind(this);
        this.changeSalaryHandler = this.changeSalaryHandler.bind(this);
        this.saveEmployee = this.saveEmployee.bind(this);
    }

    // step 3
    componentDidMount() {

        // step 4
        if (this.state.id === '_add') {
            return
        } else {
            EmployeeService.getEmployeeById(this.state.id).then((res) => {
                let employee = res.data;
                this.setState({
                    fname: employee.fname,
                    lname: employee.lname,
                    email: employee.email,
                    phoneNumber: employee.phoneNumber,
                    salary: employee.salary,
                    managerId: employee.managerId,
                    hireDate: employee.hireDate,
                    departmentId: employee.departmentId
                });
            });
        }
    }
    saveEmployee = (e) => {
        e.preventDefault();
        let employee = { fname: this.state.fname, lname: this.state.lname, email: this.state.email, salary: this.state.salary, managerId: this.state.managerId, departmentId: this.state.departmentId, hireDate: this.state.hireDate,phoneNumber:this.state.phoneNumber };
        console.log('employee => ' + JSON.stringify(employee));

        // step 5
        if (this.state.id === '_add') {
            EmployeeService.createEmployee(employee).then(res => {
                this.props.history.push('/employees');
            });
        }
    }

    changeFirstNameHandler = (event) => {
        this.setState({ fname: event.target.value });
    }

    changeLastNameHandler = (event) => {
        this.setState({ lname: event.target.value });
    }

    changeEmailHandler = (event) => {
        this.setState({ email: event.target.value });
    }

    changeManagerIdHandler = (event) => {
        this.setState({ managerId: event.target.value });
    }

    changeHireDateHandler = (event) => {
        this.setState({ hireDate: event.target.value });
    }

    changeDeptIdHandler = (event) => {
        this.setState({ departmentId: event.target.value });
    }

    changeSalaryHandler = (event) => {
        this.setState({ salary: event.target.value });
    }

    changePhoneNumberHandler = (event) => {
        this.setState({ phoneNumber: event.target.value });
    }

    cancel() {
        this.props.history.push('/employees');
    }

    getTitle() {
        if (this.state.id === '_add') {
            return <h3 className="text-center">Add Employee</h3>
        }
    }
    render() {
        return (
            <div>
                <br></br>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            {
                                this.getTitle()
                            }
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label> First Name: </label>
                                        <input placeholder="First Name" name="fname" className="form-control"
                                            value={this.state.fname} onChange={this.changeFirstNameHandler} />
                                    </div>
                                    <div className="form-group">
                                        <label> Last Name: </label>
                                        <input placeholder="Last Name" name="lname" className="form-control"
                                            value={this.state.lname} onChange={this.changeLastNameHandler} />
                                    </div>
                                    <div className="form-group">
                                        <label> Email Id: </label>
                                        <input placeholder="Email Address" name="email" className="form-control"
                                            value={this.state.email} onChange={this.changeEmailHandler} />
                                    </div>
                                    <div className="form-group">
                                        <label> Phone Number: </label>
                                        <input placeholder="Phone Number" name="phoneNumber" className="form-control"
                                            value={this.state.phoneNumber} onChange={this.changePhoneNumberHandler} />
                                    </div>
                                    <div className="form-group">
                                        <label> Manager Id: </label>
                                        <input placeholder="Manager Id" name="managerId" className="form-control"
                                            value={this.state.managerId} onChange={this.changeManagerIdHandler} />
                                    </div>
                                    <div className="form-group">
                                        <label> Salary: </label>
                                        <input placeholder="Salary" name="salary" className="form-control"
                                            value={this.state.salary} onChange={this.changeSalaryHandler} />
                                    </div>
                                    <div className="form-group">
                                        <label> Department Id: </label>
                                        <input placeholder="Department Id" name="departmentId" className="form-control"
                                            value={this.state.departmentId} onChange={this.changeDeptIdHandler} />
                                    </div>
                                    <div className="form-group">
                                        <label> Hire Date: </label>
                                        <input type="date" placeholder="Hire Date" name="hireDate" className="form-control"
                                            value={this.state.hireDate} onChange={this.changeHireDateHandler} />
                                    </div>
                                    <button className="btn btn-success" onClick={this.saveEmployee}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{ marginLeft: "10px" }}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default CreateEmployeeComponent
