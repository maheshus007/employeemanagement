package com.employee.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Builder
@Table(name="departments")
public class Department {
	@Id
	@Column(name = "department_id")
	private Long departmentId;
	@Column(name = "department_name")
	private String deptName;
	@Column(name = "manager_id")
	private String managerId;
}
