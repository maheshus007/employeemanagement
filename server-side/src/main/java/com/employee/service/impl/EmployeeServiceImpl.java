package com.employee.service.impl;

import java.util.ArrayList;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.employee.api.dto.RequestDTO;
import com.employee.repository.entity.Employee;
import com.employee.repository.repo.EmployeeRepository;
import com.employee.service.EmployeeService;

@Service
public class EmployeeServiceImpl extends EmployeeService {

	@Autowired
	private EmployeeRepository repo;

	@Override
	public Employee createEmployeeRecord(RequestDTO request) throws Exception {
		if (request.getFname() == null || request.getLname() == null) {
			throw new Exception("First name and last name cannot be null");
		}
		if (request.getFname().length() <= 2 || request.getLname().length() <= 2) {
			throw new Exception("First name and last name length should be more than two character");
		}
		if (request.getSalary() == null) {
			throw new Exception("Salary cannot be null");
		}
		if (request.getSalary() <= 0) {
			throw new Exception("Salary should be greater than 0");
		}
		if (request.getPhoneNumber() != null && !isValidMobileNo(request.getPhoneNumber())) {
			throw new Exception("Mobile number supports digits and dashes only");
		}
		Employee empData = Employee.builder().firstName(request.getFname()).lastName(request.getLname())
				.salary(request.getSalary()).phoneNumber(request.getPhoneNumber())
				.departmentId(request.getDepartmentId()).email(request.getEmail()).hireDate(request.getHireDate())
				.managerId(request.getManagerId()).build();
		return repo.save(empData);
	}

	public static boolean isValidMobileNo(String str) {
		Pattern ptrn = Pattern.compile("^\\d([0-9 -]{0,10}\\d)?$");
		Matcher match = ptrn.matcher(str);
		return (match.find() && match.group().equals(str));
	}

	@Override
	public Optional<Employee> getEmployeeRecord(Long id) {
		return repo.findById(id);
	}

	@Override
	public Iterable<Employee> getAllEmployeeRecord(Integer pageNo, Integer pageSize, String sortBy) {
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		Page<Employee> pagedResult = repo.findAll(paging);
		if (pagedResult.hasContent()) {
			return pagedResult.getContent();
		} else {
			return new ArrayList<Employee>();
		}
	}

}
