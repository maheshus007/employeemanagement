package com.employee.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.employee.api.dto.RequestDTO;
import com.employee.repository.entity.Employee;

@Service
public abstract class EmployeeService {
	
	public abstract Employee createEmployeeRecord(RequestDTO request) throws Exception;

	public abstract Optional<Employee> getEmployeeRecord(Long id);
	
	public abstract Iterable<Employee> getAllEmployeeRecord(Integer pageNo, Integer pageSize, String sortBy);
}
