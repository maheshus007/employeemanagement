package com.employee.api.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.employee.api.EmployeeResource;
import com.employee.api.dto.RequestDTO;
import com.employee.repository.entity.Employee;
import com.employee.service.EmployeeService;

@RestController
@RequestMapping("employee")
public class EmployeeResourceImpl extends EmployeeResource {

	@Autowired
	private EmployeeService empService;

	@Override
	@PostMapping
	public ResponseEntity<Employee> AddEmployee(@RequestBody RequestDTO request) throws Exception {
		return ResponseEntity.ok(empService.createEmployeeRecord(request));
	}

	@Override
	@GetMapping("/{id}")
	public ResponseEntity<Optional<Employee>> getEmployee(@PathVariable Long id) throws Exception {
		return ResponseEntity.ok(empService.getEmployeeRecord(id));
	}
	
	@Override
	@GetMapping()
	public ResponseEntity<Iterable<Employee>> getAllEmployee(@RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy) throws Exception {
		return ResponseEntity.ok(empService.getAllEmployeeRecord(pageNo,pageSize,sortBy));
	}

}
