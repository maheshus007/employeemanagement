package com.employee.api;

import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.employee.api.dto.RequestDTO;
import com.employee.repository.entity.Employee;

public abstract class EmployeeResource {

	public abstract ResponseEntity<Employee> AddEmployee(RequestDTO request) throws Exception;

	public abstract ResponseEntity<Optional<Employee>> getEmployee(Long id) throws Exception;

	public abstract ResponseEntity<Iterable<Employee>> getAllEmployee(Integer pageNo, Integer pageSize, String sortBy) throws Exception;

}
