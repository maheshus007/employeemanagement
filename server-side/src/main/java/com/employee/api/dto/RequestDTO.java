package com.employee.api.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9013598952894702775L;
	private String fname;
	private String lname;
	private Double salary;
	private String phoneNumber;
	private String email;
	private String managerId;
	private Date hireDate;
	private String departmentId;
	
}
